module.exports = {
    root : true ,
    "env": {
        "browser": true,
        "es2021": true,
        "jest": true
    },
    "extends": ["eslint:recommended", "plugin:react/recommended", "plugin:prettier/recommended"],
    "parserOptions": {
        "ecmaFeatures": {
        },
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
         "prettier", "react",
    ],
    "rules": {
        "react/react-in-jsx-scope": "off",
        "prettier/prettier": "error",
    },
     settings: {
        react: {
            
            },
    },
}
